package com.homework.invoice;

public class Product {
	
	String name;
	String sellUnit;
	double netPriceUnits;
	double grossPriceUnits;
	int unitAmount;
	double netPrice;
	double grossPrice;
	double taxPercent;
			
	void Info() {
		 
		String info = "Product name: "  + name + "\n"
				+ "Sell Unit: " + sellUnit + "\n" + 
				"Unit Gross Price: " + grossPriceUnits+ "\n" + 
				"Unit Net Price:	 " + netPriceUnits + "\n" + 
				"Final Gross Price: " + grossPrice + "\n" +
				"Final Net Price: " + netPrice + "\n"  +
				"Tax Percent: "		+ taxPercent+ "\n" +
				"Units Amount: " + unitAmount;
			
		
		System.out.println(info);
}
}
